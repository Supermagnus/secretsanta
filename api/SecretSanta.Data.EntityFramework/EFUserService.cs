﻿using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SecretSanta.Data.EntityFramework
{
    public class EFUserService : IUserService
    {
        private readonly SecretSantaContext _context;

        public EFUserService(SecretSantaContext context)
        {
            this._context = context;
        }

        public void DeleteUser(string userName)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(long userId)
        {
            throw new NotImplementedException();
        }

        public void DeleteWish(long wishId, long userId)
        {
            throw new NotImplementedException();
        }

        public IList<User> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public User GetUser(string userName)
        {
            throw new NotImplementedException();
        }

        public User GetUser(long userId)
        {
            throw new NotImplementedException();
        }

        public IList<UserMessage> GetUserMessages(params MessageFilter[] filters)
        {
            throw new NotImplementedException();
        }

        public IList<Wish> GetWishes(long userId)
        {
            throw new NotImplementedException();
        }

        public int SaveUser(User user)
        {
            throw new NotImplementedException();
        }

        public void SaveUserMessage(UserMessage message, long fromUserId, long toUserId)
        {
            throw new NotImplementedException();
        }

        public int SaveWish(Wish wish, long userId)
        {
            throw new NotImplementedException();
        }
    }
}
