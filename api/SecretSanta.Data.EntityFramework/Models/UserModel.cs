using SecretSanta.Core.Domain;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class UserModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName{ get; set; }

        public string Email{ get; set; }

        public DateTime? LastLoginDate { get; set; }
        public UserRole Role { get; set; }
        public string Password { get; set; }

        public User ToUser()
        {
            return new User()
            {
                Id = Id,
                Email = Email,
                FirstName = FirstName,
                LastName = LastName,
                Password = Password,
                Role = Role,
                UserName = UserName,
                LastLoginDate = LastLoginDate
            };
        }
    }

}