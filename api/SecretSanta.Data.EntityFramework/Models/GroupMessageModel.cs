using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class GroupMessageModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long ToGroupId { get; set; }
        public long FromUserId { get; set; }

        public DateTime CreateDate { get; set; }
        public string Message{ get; set; }

    }

}