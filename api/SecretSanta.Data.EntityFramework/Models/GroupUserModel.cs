﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    
    public class GroupUserModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long GroupId { get; set; }
        public long UserId { get; set; }
        public bool IsAdmin { get; set; }

    }

}
