using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class GroupModel
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime? Started { get; set; }
        public string BlacklistedPairs { get; set; }
        public string GeneratedPairs { get; set; }
        
    }


  
}