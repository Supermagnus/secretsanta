﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Data.EntityFramework;

namespace SecretSanta.Core.Infrastructure
{
    public static class EFInstaller
    {
        public static IServiceCollection AddSecretSantaEFServices(this IServiceCollection services, string secretSantaConnectionString)
        {
            services.AddDbContext<SecretSantaContext>(opts => opts.UseSqlServer(secretSantaConnectionString));
            services.AddScoped<SecretSantaContext>();
            services.AddScoped<IUserService, EFUserService>();
            services.AddScoped<IGroupService, EFGroupService>();
            return services;
        }
    }
}
