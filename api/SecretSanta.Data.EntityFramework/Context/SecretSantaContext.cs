﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecretSanta.Data.EntityFramework
{
    public class SecretSantaContext : DbContext
    {
        public DbSet<UserModel> Users { get; set; }

        public DbSet<GroupModel> Groups { get; set; }

        public DbSet<GroupMessageModel> GroupMessages { get; set; }
        public DbSet<GroupUserModel> GroupUserRelation{ get; set; }
        public DbSet<UserMessageModel> UserMessages { get; set; }
        public DbSet<WishModel> Wishes{ get; set; }


        public SecretSantaContext(DbContextOptions options) : base(options)
        {
        }
    }

}
