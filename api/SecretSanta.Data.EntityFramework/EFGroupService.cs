﻿using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using System;
using System.Collections.Generic;

namespace SecretSanta.Data.EntityFramework
{
    public class EFGroupService : IGroupService
    {
        private readonly SecretSantaContext context;

        public EFGroupService(SecretSantaContext context)                   
        {
            this.context = context;
        }

        public void DeleteGroup(string groupName)
        {
            throw new NotImplementedException();
        }

        public void DeleteGroup(long groupId)
        {
            throw new NotImplementedException();
        }

        public IList<UserPair> GenerateUserPairs(Group group)
        {
            throw new NotImplementedException();
        }

        public IList<Group> GetAdministratedGroups(long usedId)
        {
            throw new NotImplementedException();
        }

        public IList<Group> GetAllGroups()
        {
            throw new NotImplementedException();
        }

        public Group GetGroup(string groupName)
        {
            throw new NotImplementedException();
        }

        public Group GetGroup(long groupId)
        {
            throw new NotImplementedException();
        }

        public IList<UserMessage> GetGroupMessages(long groupId, int maxMessages)
        {
            throw new NotImplementedException();
        }

        public IList<Group> GetGroupsForUser(string userName)
        {
            throw new NotImplementedException();
        }

        public IList<Group> GetGroupsForUser(long userId)
        {
            throw new NotImplementedException();
        }

        public long SaveGroup(Group group)
        {
            throw new NotImplementedException();
        }

        public void SaveGroupMessage(UserMessage message, long fromUserId, long toGroupId)
        {
            throw new NotImplementedException();
        }
    }
}
