﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;

namespace SecretSanta.Core.ApplicationServices.Interfaces
{
    public interface IGroupService
    {
        /// <summary>
        /// Returns the Group id
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        long SaveGroup(Group group);

        Group GetGroup(string groupName);
        Group GetGroup(long groupId);

        IList<Group> GetAllGroups();
        
        IList<Group> GetGroupsForUser(string userName);
        IList<Group> GetGroupsForUser(long userId);
        void DeleteGroup(string groupName);
        void DeleteGroup(long groupId);

        /// <summary>
        /// Fetches the groups where the given userId is an administrator of
        /// </summary>
        /// <param name="usedId"></param>
        /// <returns></returns>
        IList<Group> GetAdministratedGroups(long usedId);

        void SaveGroupMessage(UserMessage message, long fromUserId, long toGroupId);
        IList<UserMessage> GetGroupMessages(long groupId, int maxMessages);
       
        IList<UserPair> GenerateUserPairs(Group group);
    }
}