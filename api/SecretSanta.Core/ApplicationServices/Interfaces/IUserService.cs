﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using SecretSanta.Core.Domain;

namespace SecretSanta.Core.ApplicationServices.Interfaces
{
    public interface IUserService
    {
        int SaveUser(User user);
        User GetUser(string userName);
        User GetUser(long userId);
        IList<User> GetAllUsers();
        void DeleteUser(string userName);
        void DeleteUser(long userId);

        void SaveUserMessage(UserMessage message, long fromUserId, long toUserId);
        /// <summary>
        /// Get all messages written to the given user Id. If you only want to see messages 
        /// from a conversation between two users, specify a fromUserId
        /// </summary>
        /// <returns></returns>
        IList<UserMessage> GetUserMessages(params MessageFilter[] filters);

        int SaveWish(Wish wish, long userId);
        void DeleteWish(long wishId, long userId);
        IList<Wish> GetWishes(long userId);
    }
}
