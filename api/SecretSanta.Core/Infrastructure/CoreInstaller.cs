﻿using Microsoft.Extensions.DependencyInjection;
using SecretSanta.Core.ApplicationServices.Implementations;
using SecretSanta.Core.ApplicationServices.Interfaces;


namespace SecretSanta.Core.Infrastructure
{
    public static class CoreRegistry
    {
        public static IServiceCollection AddSecretSantaCoreServices(this IServiceCollection services)
        {
            services.AddSingleton<EmailSettings>();
            services.AddSingleton<IEmailService, SmtpEmailService>();
            
            return services;
        }
    }
    //public class CoreRegistry : Registry
    //{

    //    public CoreRegistry()
    //    {
    //        ForSingletonOf<EmailSettings>().Use<EmailSettings>();
    //        ForSingletonOf<IEmailService>().Use<SmtpEmailService>();
    //    }
    //}
}
