﻿namespace SecretSanta.Core.Domain
{
    public enum UserRole
    {
        User,
        Admin
    }
}