﻿using System;

namespace SecretSanta.Core.Domain
{
    public class UserMessage
    {
        public string UserName { get; set; }
        public DateTime CreateDate { get; set; }
        public string Message { get; set; }
    }
}