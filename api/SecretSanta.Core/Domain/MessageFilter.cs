﻿namespace SecretSanta.Core.Domain
{
    public class MessageFilter
    {
        /// <summary>
        /// Get messages where this user is the receiver
        /// </summary>
        public long? ToUserId { get; set; }

        /// <summary>
        /// Get messages where this user is the send
        /// </summary>
        public long? FromUserId { get; set; }

        /// <summary>
        /// Limit the amount of messages to retrieve
        /// </summary>
        public int MaxMessages { get; set; }

        public static MessageFilter[] CreateUserConversationFilter(long user1Id, long user2Id)
        {
            var filters = new MessageFilter[2]
            {
                new MessageFilter()
                {
                    FromUserId = user1Id,
                    ToUserId = user2Id,
                    MaxMessages = 100
                },
                new MessageFilter()
                {
                    FromUserId = user2Id,
                    ToUserId = user1Id,
                    MaxMessages = 100
                }
            };
            return filters;
        }
}
}