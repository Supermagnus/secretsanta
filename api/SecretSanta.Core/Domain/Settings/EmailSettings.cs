﻿using System.Configuration;

namespace SecretSanta.Core
{
    public class EmailSettings
    {
        public string SmtpServer { get; set; }
        public int SmtpServerPort { get; set; }
        public string SendingEmail { get; set;   }
        public string EmailPassword { get; set; }
    }
}
