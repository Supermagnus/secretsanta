﻿    namespace SecretSanta.Core.Domain
{
    public class UserPair
    {
        public long SecretSantaId { get; set; }
        public long ReceiverId { get; set; }
    }
}