﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;
using SecretSanta.WebUI.Controllers.Game.ViewModels;

namespace SecretSanta.RazorWebUI.Controllers.Game.ViewModels
{
    public class UserWishlistPresenterViewModel : GameBaseViewModel
    {
        public User LoggedInUser { get; set; }
        public User SecretSantaUser { get; set; }
        public IList<Wish> LoggedInUserWishlist { get; set; }
        public AnonUserChatViewModel AnonUserChatViewModel { get; set; }
    }
}