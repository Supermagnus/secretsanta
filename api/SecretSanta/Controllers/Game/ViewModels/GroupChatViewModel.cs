﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;
using SecretSanta.WebUI.Controllers.Game.ViewModels;

namespace SecretSanta.RazorWebUI.Controllers.Game.ViewModels
{
    public class GroupChatViewModel : GameBaseViewModel
    {
        public IList<UserMessage> Messages { get; set; }
        public string SendingUserName { get; set; }
        public string GroupName { get; set; }
        public int GroupId { get; set; }
    }
}