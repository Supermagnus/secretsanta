﻿namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public abstract class GameBaseViewModel
    {
        public string CurrentGroup { get; set; }
        public string CurrentTab { get; set; }

        public void SetBaseViewModel(GameBaseViewModel baseModel)
        {
            baseModel.CurrentGroup = CurrentGroup;
            baseModel.CurrentTab = CurrentTab;
        }
    }
}