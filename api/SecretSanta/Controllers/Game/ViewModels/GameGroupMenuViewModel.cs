﻿using System.Collections.Generic;

namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class GameGroupMenuViewModel
    {
        public string CurrentGroup { get; set; }
        public IList<string> AvailableGroupNames { get; set; }
    }
}