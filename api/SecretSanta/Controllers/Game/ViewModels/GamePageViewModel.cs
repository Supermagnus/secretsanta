﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecretSanta.Core.Domain;
using SecretSanta.RazorWebUI.Controllers.Game.ViewModels;

namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class GamePageViewModel : GameBaseViewModel
    {
        public User LoggedInUser { get; set; }
        public ReceivingUserPresenterViewModel ReceivingUserPresenterViewModel { get; set; }
        public MenuViewModel MenuViewModel { get; set; }        
        public GroupChatPresenterViewModel GroupChatPresenterViewModel { get; set; }
        public UserWishlistPresenterViewModel UserWishlistPresenterViewModel { get; set; }
    }
}