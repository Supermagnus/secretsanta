﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;

namespace SecretSanta.RazorWebUI.Controllers.Game.ViewModels
{
    public class GroupInformationModalViewModel
    {
        public Group Group { get; set; }
        public string ModalId { get; set; }
        public IList<User> Users { get; set; }
    }
}