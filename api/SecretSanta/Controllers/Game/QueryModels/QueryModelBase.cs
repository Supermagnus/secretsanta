﻿namespace SecretSanta.WebUI.Controllers.Game.QueryModels
{
    public abstract class QueryModelBase
    {
        public string CurrentGroup{ get; set; }
        public string CurrentTab{ get; set; }

    }
}