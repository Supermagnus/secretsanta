﻿namespace SecretSanta.RazorWebUI.Controllers.Admin
{
    public class UserNamePair
    {
        public string SecretSantaUserName { get; set; }
        public string  ReceiverUserName { get; set; }
    }
}