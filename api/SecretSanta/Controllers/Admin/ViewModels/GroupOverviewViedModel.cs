﻿using System.Collections.Generic;

namespace SecretSanta.RazorWebUI.Controllers.Admin.ViewModels
{
    public class GroupOverviewViedModel
    {
        public IList<GroupViewModel> GroupViewModels { get; set; }

    }
}