﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;

namespace SecretSanta.RazorWebUI.Controllers.Admin.ViewModels
{
    public class UserOVerviewModelView
    {
        public IList<User> Users { get; set; } 
    }
}