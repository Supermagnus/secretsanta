﻿using System;
using System.Security.Claims;
using SecretSanta.Core.Domain;

namespace SecretSanta.RazorWebUI.Helpers
{
    public static class ClaimsExtensions {

        public static bool IsAdmin(this ClaimsPrincipal user)
        {
            if (user.HasClaim(x => x.Type == ClaimTypes.Role && Enum.Parse<UserRole>(x.Value) == UserRole.Admin))
                return true;
            return false;
        }
        

        
    }
}