﻿//    using System.Collections.Generic;
//    using System.Diagnostics;
//    using FluentAssertions;
//    using Microsoft.VisualStudio.TestTools.UnitTesting;
//using SecretSanta.Core.Domain;
//using SecretSanta.Data.FileSystem;
//using SecretSanta.Data.FileSystem.Settings;

//namespace SecretSanta.Tests.Data.FileSystem
//{
//    [TestClass]
//    public class FileSystemGroupServiceTests
//    {

//        private FileSystemGroupService CreateService()
//        {
//            return new FileSystemGroupService(new FileSystemSettings()
//            {
//                FolderPath = @"C:\1.Jobb\TestData"
//            });
//        }

//        [TestMethod]
//        public void CanGenerateEvenGroups()
//        {
//            var users = GenerateUsers();

//            var service = CreateService();
//            var group = new Group() {Users = GenerateUsers(), Name = "test"};
//            var pairs = service.GenerateUserPairs(group);

//            pairs.Should().NotBeNull();
//            pairs.Should().HaveCount(users.Count);

//        }

//        [TestMethod]
//        public void CanRespectBlacklist()
//        {
//            var users = GenerateUsers();

//            var service = CreateService();
//            var group = new Group() { Users = GenerateUsers(), Name = "test" };
//            group.BlacklistedPairs = new List<UserPair>();
//            AddTwoWayUserPairToList(group.BlacklistedPairs, "magnus","sanna");
//            AddTwoWayUserPairToList(group.BlacklistedPairs, "carin", "jannah");
//            AddTwoWayUserPairToList(group.BlacklistedPairs, "bodil", "lars");
//            var pairs = service.GenerateUserPairs(group);

//            pairs.Should().NotBeNull();
//            pairs.Should().HaveCount(users.Count);
//        }

//        private IList<User> GenerateUsers()
//        {
//            return new List<User>()
//            {
//                new User()
//                {
//                    UserName = "Magnus"
//                },
//                new User()
//                {
//                    UserName = "Sanna"
//                },
//                new User()
//                {
//                    UserName = "Carin"
//                },
//                new User()
//                {
//                    UserName = "Lars"
//                },
//                new User()
//                {
//                    UserName = "Bodil"
//                },
//                new User()
//                {
//                    UserName = "Jannah"
//                },
//                new User()
//                {
//                    UserName = "Pontus"
//                }
//            };
//        }

//        private void AddTwoWayUserPairToList( IList<UserPair> list, string user1, string user2)
//        {
//           list.Add(new UserPair() {ReceiverId = user1, SecretSantaId = user2});
//            list.Add(new UserPair() { ReceiverId = user2, SecretSantaId = user1 });
//        } 
//    }
//}