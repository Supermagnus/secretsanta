﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecretSanta.Core.Domain;
using SecretSanta.Data.FileSystem;
using SecretSanta.Data.FileSystem.Settings;
using SecretSanta.Data.Sql;
using SecretSanta.Data.Sql.Databases;
using SecretSanta.Data.Sql.Settings;

namespace SecretSanta.Tests.Data.Sql
{
    [TestClass]
    public class SqlUserServiceTests
    {

        private SqlUserService CreateService()
        {
            return new SqlUserService(new SecretSantaDatabase(new SqlSettings
            {
                SecretSantaConnectionString = @"Server=localhost\SQLEXPRESS;Database=HemligaJultomten;Trusted_Connection=True;"
            }));
        }




        [TestMethod]
        public void CanCreate()
        {
            var service = CreateService();
        }


        [TestMethod]
        public void CanSaveAndLoadUser()
        {
            var service = CreateService();
            var user = new User()
            {
                Email = "test@test.com",
                FirstName = "Magnus",
                LastName = "Månsson",
                Password = "masd",
                UserName = "mansso"
            };
            var id = service.SaveUser(user);

            var retrievedUser = service.GetUser(id);
            retrievedUser.ShouldBeEquivalentTo(user);
        }

        [TestMethod]
        public void CanSaveAndDeleteUser()
        {
            var service = CreateService();
            var user = new User()
            {
                Email = "test@test.com",
                FirstName = "Magnus",
                LastName = "Månsson",
                Password = "masd",
                UserName = "mansso"
            };
            var id = service.SaveUser(user);

            var retrievedUser = service.GetUser(id);
            retrievedUser.ShouldBeEquivalentTo(user);
            service.DeleteUser(user.Id);
            retrievedUser = service.GetUser(id);
            retrievedUser.Should().BeNull();
        }


        [TestMethod]
        public void CanSaveAndLoadMessage()
        {
            var service = CreateService();
            var user1 = new User()
            {
                Email = "test@test.com",
                FirstName = "Magnus",
                LastName = "Månsson",
                Password = "masd",
                UserName = "mansso"
            };

            var user2 = new User()
            {
                Email = "test@test.com",
                FirstName = "Magnus",
                LastName = "Månsson",
                Password = "masd",
                UserName = "manssoMan"
                }; 
            var id1 = service.SaveUser(user1);
            var id2 = service.SaveUser(user2);

            var message = new UserMessage()
            {
                CreateDate = DateTime.Now,
                Message = "hej där!",
                UserName = user1.UserName
            };

            service.SaveUserMessage(message, id1,id2);

            var retrievedMessage = service.GetUserMessages(new MessageFilter()
            {
                FromUserId = id1,
                ToUserId = id2
            });


            retrievedMessage.Contains(message);

        }



    }
}
