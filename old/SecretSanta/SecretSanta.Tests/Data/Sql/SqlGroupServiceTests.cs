﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SecretSanta.Core.Domain;
using SecretSanta.Data.FileSystem;
using SecretSanta.Data.FileSystem.Settings;
using SecretSanta.Data.Sql;
using SecretSanta.Data.Sql.Databases;
using SecretSanta.Data.Sql.Settings;

namespace SecretSanta.Tests.Data.Sql
{
    [TestClass]
    public class SqlGroupServiceTests
    {

        private SqlGroupService CreateService()
        {
            return new SqlGroupService(new SecretSantaDatabase(new SqlSettings
            {
                SecretSantaConnectionString = @"Server=localhost\SQLEXPRESS;Database=HemligaJultomten;Trusted_Connection=True;"

            }));
        }

        private SqlUserService CreateUserService()
        {
            return new SqlUserService(new SecretSantaDatabase(new SqlSettings
            {
                SecretSantaConnectionString = @"Server=localhost\SQLEXPRESS;Database=HemligaJultomten;Trusted_Connection=True;"
            }));
        }


        [TestMethod]
        public void CanCreate()
        {
            var service = CreateService();
        }


        [TestMethod]
        public void CanSaveAndLoadEmptyGroup()
        {
            var service = CreateService();
            var group = new Group()
            {
                Name = "Test"
            };
            var id = service.SaveGroup(group);

            var retrieved = service.GetGroup(id);
            retrieved.ShouldBeEquivalentTo(group);
        }

        [TestMethod]
        public void CanSaveAndDeleteEmptyGroup()
        {
            var service = CreateService();
            var group = new Group()
            {
                Name = "TestDelete"
            };
            var id = service.SaveGroup(group);


            var retrieved = service.GetGroup(id);
            retrieved.ShouldBeEquivalentTo(group);

            service.DeleteGroup(id);
            retrieved= service.GetGroup(id);
            retrieved.Should().BeNull();
        }

        [TestMethod]
        public void CanSaveAndLoadGroupWithMembers()
        {
            var service = CreateService();
            var user1 = AddNewUser();
            var user2 = AddNewUser();
            var group = new Group()
            {
                Name = "Testme",
                UserIds = new List<int>() { user1.Id, user2.Id },
                AdminUserIds = new List<int>() { user1.Id }
            };
        
            service.SaveGroup(group);
            var retrievedGroup = service.GetGroup(group.Id);
            retrievedGroup.ShouldBeEquivalentTo(group);

        }

        [TestMethod]
        public void CanSaveAndLoadMessage()
        {
            var service = CreateService();
            var user1 = AddNewUser();
            var user2 = AddNewUser();
            var group = new Group()
            {
                Name = "TestDelete",
                UserIds = new List<int>() { user1.Id, user2.Id },
                AdminUserIds = new List<int>(){user1.Id}
            };
            var message1 = new UserMessage()
            {
                CreateDate = DateTime.Now,
                Message = "hej där!",
                UserName = user1.UserName
            };

            var message2 = new UserMessage()
            {
                CreateDate = DateTime.Now,
                Message = "Tja!",
                UserName = user2.UserName
            };
            service.SaveGroup(group);

            service.SaveGroupMessage(message1,user1.Id, group.Id);
            service.SaveGroupMessage(message2, user2.Id, group.Id);

            var retrievedMessages = service.GetGroupMessages(group.Id);

            

            retrievedMessages.Contains(message1);
            retrievedMessages.Contains(message2);

        }

        private User AddNewUser()
        {
            var service = CreateUserService();
            var user = new User()
            {
                Email = "test@test.com",
                FirstName = "Magnus",
                LastName = "Månsson",
                Password = "masd",
                UserName = "mansso_" +Guid.NewGuid()
            };

            service.SaveUser(user);
            return user;

        }



    }
}
