﻿$(document).ready(function () {

    theGame.init();


    $(document).snowfall({
        flakeCount: 100,
        maxSpeed: 10,
        minSize: 1,
        maxSize: 10,
        round: true,
        collection: '.snowfallCollector'
        
        
    });

    $("#clear").click(function () {
        $(document).snowfall('clear'); // How you clear
    });
   
});