﻿var theGame  = function () {
    var init = function () {


        
        $("#unwrap-ss").click(function () {
            $.ajax({
                url: '/Game/StartGameForUser/',
                contentType: 'application/html; charset=utf-8',
                type: 'GET',
                dataType: 'html',
                data: {
                }

            })
            .success(function (result) {
            })
            .error(function (xhr, status) {
                alert(xhr.responseText);
            });



        });

        $(".chat").animate({ scrollTop: $(document).height() }, "fast");

        $('#gameTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show')
            $(".chat").animate({ scrollTop: $(document).height() }, "fast");
        });


        function addUrlParam(param, value) {
            var url = location.href;
            if (location.search.indexOf(param) != -1) return url;
            var hash = location.hash, sep = url.indexOf('?') == -1 ? '?' : '&amp;';
            return url.replace(hash, '') + sep + encodeURIComponent(param) + (value ? '=' + encodeURIComponent(value) : '') + hash;
        }

        var glow = $('.receiving-user-text');
        setInterval(function () {
            glow.toggleClass('glow');
        }, 2000);
     
    };

    return {
        // set up which functions should be public
        init: init
    };
} ();