﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Game.Domain
{
    public class CurrentGroup
    {
        public Group Group { get; set; }
        public IList<UserMessage> GroupMessages { get; set; }
    }
}