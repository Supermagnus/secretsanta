﻿namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class GroupChatPresenterViewModel
    {
        public GroupChatViewModel GroupChatViewModel { get; set; }
        public GroupInformationModalViewModel GroupInformationModalViewModel { get; set;}
    }
}