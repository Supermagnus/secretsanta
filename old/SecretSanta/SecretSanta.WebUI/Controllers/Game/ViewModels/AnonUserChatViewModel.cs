﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;
using SecretSanta.WebUI.Controllers.Game.Domain;

namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class AnonUserChatViewModel : GameBaseViewModel
    {
        public AnonUserChatViewModel()
        {
            Messages = new List<Message>();
        }
        public IList<Message> Messages { get; set; } 
        public User LoggedInUser { get; set; }
        public User ReceiverUser { get; set; }
    }
}