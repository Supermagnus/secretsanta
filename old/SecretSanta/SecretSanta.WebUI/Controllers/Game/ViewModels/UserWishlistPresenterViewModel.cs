﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class UserWishlistPresenterViewModel : GameBaseViewModel
    {
        public User LoggedInUser { get; set; }
        public User SecretSantaUser { get; set; }
        public IList<Wish> LoggedInUserWishlist { get; set; }
        public AnonUserChatViewModel AnonUserChatViewModel { get; set; }
    }
}