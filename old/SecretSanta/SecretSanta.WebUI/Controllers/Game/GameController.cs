﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.WebUI.Attributes;
using SecretSanta.WebUI.Controllers.Game.Domain;
using SecretSanta.WebUI.Controllers.Game.ModelMappers;
using SecretSanta.WebUI.Controllers.Game.QueryModels;
using SecretSanta.WebUI.Controllers.Game.ViewModels;

namespace SecretSanta.WebUI.Controllers.Game
{
    public class GameController : Controller
    {
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private readonly IGamePageModelMapper _gamePageMapper;


        public GameController(IUserService userService, IGroupService groupService, IGamePageModelMapper gamePageMapper){
            _userService = userService;
            _groupService = groupService;
            _gamePageMapper = gamePageMapper;
        }


        [RequireLogin]
        public ActionResult GamePage(string currentGroup = "",string currentTab ="")
        {
            var loggedInUser = GetUserFromCookie();
            if (loggedInUser == null)
                return LoginPage();
            var availableGroups = _groupService.GetGroupsForUser(loggedInUser.UserName);
            if (!availableGroups.Any() || !availableGroups.Any(x => x.IsStarted()))
                return View("NoGroup", loggedInUser);
            
            var selectedGroup = new CurrentGroup()
            {
                Group = availableGroups.FirstOrDefault(x => x.Name == currentGroup) ?? availableGroups.FirstOrDefault()
            };
            selectedGroup.GroupMessages = _groupService.GetGroupMessages(selectedGroup.Group.Id, 150);
            var linkedUser = GetLinkedUsers(loggedInUser, selectedGroup.Group);

            var model = new GamePageViewModel
            {
                CurrentGroup = selectedGroup.Group.Name,
                CurrentTab = currentTab,
                GroupChatPresenterViewModel =
                    _gamePageMapper.MapGroupChatPresenter(
                    loggedInUser, selectedGroup.Group, selectedGroup.GroupMessages, 
                    selectedGroup.Group.UserIds.Select(userId => _userService.GetUser(userId)).ToList()),
                LoggedInUser = loggedInUser,
                MenuViewModel = new MenuViewModel()
                {
                    GameGroupMenuViewModel = _gamePageMapper.MapGameGroupMenu(availableGroups, selectedGroup.Group)
                },
                ReceivingUserPresenterViewModel =
                    _gamePageMapper.MapReceivingUserViewModel(linkedUser, selectedGroup.Group,
                        _userService.GetWishes(linkedUser.ReceiverUser.Id)),
                UserWishlistPresenterViewModel =
                    _gamePageMapper.MapUserWishlist(linkedUser,_userService.GetWishes(loggedInUser.Id))
            };
            return View("GamePage",model);
        }



        [RequireLogin]
        public string StartGameForUser()
        {
            var user = GetUserFromCookie();
            user.LastLoginDate = DateTime.Now;
            _userService.SaveUser(user);
            return "Kör på!";
        }

        public ActionResult LoginPage()
        {
            return View("LoginPage");
        }


        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            SetLoginCookie(userName,password);
           
            return RedirectToAction("GamePage");
        }


        public ActionResult Logout()
        {
            ClearUserCookie();
            return RedirectToAction("LoginPage");
        }





        [RequireLogin]
        public JsonResult SaveOrUpdateWish(WishQueryModel wishQueryModel)
        {
            var loggedInUser = GetUserFromCookie();
            var wish = new Wish()
            {
                Description = wishQueryModel.Wish,
                Id = wishQueryModel.WishId
            };

            _userService.SaveWish(wish, loggedInUser.Id);
            return Json("ok");
        }

        [RequireLogin]
        public ActionResult AddWish(WishQueryModel wishQueryModel)
        {
            var loggedInUser = GetUserFromCookie();
            var wish = new Wish()
            {
                Description = wishQueryModel.Wish,
                Id = wishQueryModel.WishId
            };
            _userService.SaveWish(wish, loggedInUser.Id);
            return RedirectToAction("GamePage", new { currentGroup = wishQueryModel.CurrentGroup, currentTab = wishQueryModel.CurrentTab });
        }

        [RequireLogin]
        public ActionResult DeleteWish(WishQueryModel wishQueryModel)
        {
            var loggedInUser = GetUserFromCookie();

            _userService.DeleteWish(wishQueryModel.WishId,loggedInUser.Id);
            return RedirectToAction("GamePage", new { currentGroup = wishQueryModel.CurrentGroup, currentTab = wishQueryModel.CurrentTab });

        }

       
        [RequireLogin]
        public ActionResult SendUserMessage(UserMessageQueryModel messageQueryModel)
        {
            var loggedInUser = GetUserFromCookie();

            _userService.SaveUserMessage(
                new UserMessage()
                {
                    CreateDate = DateTime.Now,
                    Message = messageQueryModel.Message,
                    UserName = loggedInUser.UserName
                },
                loggedInUser.Id,
                messageQueryModel.ReceiverUserId);
            
            return RedirectToAction("GamePage",  new {messageQueryModel.CurrentGroup, messageQueryModel.CurrentTab});

        }

        [RequireLogin]
        public ActionResult SendGroupMessage(GroupMessageQueryModel groupMessage)
        {
            var group = _groupService.GetGroup(groupMessage.GroupId);
            var loggedInUser = GetUserFromCookie();

            if (group == null || loggedInUser == null)
                return RedirectToAction("GamePage");
            _groupService.SaveGroupMessage(
                new UserMessage()
                {
                    CreateDate = DateTime.Now,
                    UserName = loggedInUser.UserName,
                    Message = groupMessage.Message
                },
                loggedInUser.Id,
                group.Id);
            
            return RedirectToAction("GamePage", new { groupMessage.CurrentGroup, groupMessage.CurrentTab });

        }




        private void SetLoginCookie(string username, string password)
        {
            var cookie = new HttpCookie("SecretSanta");
            cookie["UserName"] = username;
            cookie["Password"] = password;
            cookie.Expires = DateTime.Now.AddHours(24);

            HttpContext.Response.AppendCookie(cookie);

        }
    
        private void ClearUserCookie()
        {
            HttpContext.Response.AppendCookie(new HttpCookie("SecretSanta")
            {
                Expires = DateTime.Now.AddHours(-10)
            });
        }

        private User GetUserFromCookie()
        {
            var name = HttpContext.Request.Cookies["SecretSanta"]["UserName"];
            var pass = HttpContext.Request.Cookies["SecretSanta"]["Password"];
            var user = _userService.GetUser(name);
            if (user == null)
                return null;
            if (user.Password != pass)
                return null;

            if (!user.LastLoginDate.HasValue || (DateTime.Now - user.LastLoginDate.Value).TotalMinutes > 5)
            {
                user.LastLoginDate = DateTime.Now;
                _userService.SaveUser(user);
            }
            return user;
        }

        private LinkedUsers GetLinkedUsers(User loggedInUser, Group group)
        {
            var secretSantaUser = _userService.GetUser(group.GetPairWhereUserIsReceiver(loggedInUser.Id).SecretSantaId);
            var receiverUser = _userService.GetUser((group.GetPairWhereUserIsSecretSanta(loggedInUser.Id).ReceiverId));
            var secretSantaChat =
                _userService.GetUserMessages(MessageFilter.CreateUserConversationFilter(loggedInUser.Id,
                    secretSantaUser.Id));
            var receiverChat =
                _userService.GetUserMessages(MessageFilter.CreateUserConversationFilter(loggedInUser.Id,
                    receiverUser.Id));
            return new LinkedUsers()
            {
                LoggedInUser = loggedInUser,
                ReceiverUser = receiverUser,
                SecretSantaUser = secretSantaUser,
                SecretSantaConversation = GetMessageConversation(secretSantaChat, secretSantaUser.UserName),
                ReceivingUserConversation = GetMessageConversation(receiverChat)
            };
        }
        private IList<Message> GetMessageConversation( IList<UserMessage> messages,string anonymizeUsername ="" )
        {
            return messages.Select(
                msg => new Message()
                {
                    Sender = string.IsNullOrEmpty(anonymizeUsername) ? msg.UserName
                        : msg.UserName == anonymizeUsername ? "??" : msg.UserName,
                    Text = msg.Message,
                    CreateDate = msg.CreateDate
                }).ToList();
        } 
    }
}
