﻿using System.Collections.Generic;
using System.Linq;
using SecretSanta.Core.Domain;
using SecretSanta.WebUI.Controllers.Game.Domain;
using SecretSanta.WebUI.Controllers.Game.ViewModels;
using SecretSanta.WebUI.Infrastructure;

namespace SecretSanta.WebUI.Controllers.Game.ModelMappers
{
    public interface IGamePageModelMapper : IModelMapper
    {
         ReceivingUserPresenterViewModel MapReceivingUserViewModel(LinkedUsers linkedUsers, Group selectedGroup, IList<Wish> receivingUserWishlist);
        UserWishlistPresenterViewModel MapUserWishlist(LinkedUsers linkedUsers, IList<Wish> loggedInUserWishlist );
        GroupChatPresenterViewModel MapGroupChatPresenter(User loggedInUser, Group group, IList<UserMessage> groupMessages, IList<User> users);
        GroupChatViewModel MapGroupChat(User loggedInUser, Group group, IList<UserMessage> groupMessages);
        GameGroupMenuViewModel MapGameGroupMenu(IList<Group> availableGroups, Group selectedGroup);
        
        
    }

    public class GamePageModelMapper : IGamePageModelMapper
    {
       
        public ReceivingUserPresenterViewModel MapReceivingUserViewModel(LinkedUsers linkedUsers, Group selectedGroup, IList<Wish> receivingUserWishlist)
        {
            if (!selectedGroup.Started.HasValue)
                return new ReceivingUserPresenterViewModel() { GroupName = selectedGroup.Name, AnonUserChatViewModel = new AnonUserChatViewModel() };
            return new ReceivingUserPresenterViewModel()
            {
                GroupName = selectedGroup.Name,
                ReceivingUser = linkedUsers.ReceiverUser,
                AnonUserChatViewModel = new AnonUserChatViewModel()
                {
                    Messages = linkedUsers.ReceivingUserConversation.OrderBy(x => x.CreateDate).ToList(),
                    ReceiverUser = linkedUsers.ReceiverUser,
                    LoggedInUser = linkedUsers.LoggedInUser
                },
                ReceivingUserWishlist = receivingUserWishlist
            };
        }

        public UserWishlistPresenterViewModel MapUserWishlist(LinkedUsers linkedUsers, IList<Wish> loggedInUserWishlist)
        {
            if (linkedUsers.ReceiverUser == null || linkedUsers.SecretSantaUser == null)
                return new UserWishlistPresenterViewModel()
                {
                    LoggedInUser = linkedUsers.LoggedInUser,
                    AnonUserChatViewModel = new AnonUserChatViewModel(),
                    LoggedInUserWishlist = loggedInUserWishlist
                };
            return new UserWishlistPresenterViewModel()
            {
                LoggedInUser = linkedUsers.LoggedInUser,
                SecretSantaUser = linkedUsers.SecretSantaUser,
                AnonUserChatViewModel = new AnonUserChatViewModel()
                {
                    Messages = linkedUsers.SecretSantaConversation.OrderBy(x => x.CreateDate).ToList(),
                    ReceiverUser = linkedUsers.SecretSantaUser,
                    LoggedInUser = linkedUsers.LoggedInUser
                },
                LoggedInUserWishlist = loggedInUserWishlist
            };
        }

        public GroupChatPresenterViewModel MapGroupChatPresenter(User loggedInUser, Group group, IList<UserMessage> groupMessages, IList<User> users)
        {
            return new GroupChatPresenterViewModel()
            {
                GroupInformationModalViewModel = new GroupInformationModalViewModel()
                {
                    Group = group,
                    ModalId = "GroupChatModal",
                    Users = users
                },
                GroupChatViewModel = MapGroupChat(loggedInUser,group, groupMessages)
            };
        }

        public GroupChatViewModel MapGroupChat(User loggedInUser, Group group,  IList<UserMessage> groupMessages )
        {
            return new GroupChatViewModel()
            {
                SendingUserName = loggedInUser.UserName,
                GroupName = group.Name,
                Messages = groupMessages.OrderBy(x=>x.CreateDate).ToList(),
                GroupId = group.Id
            };
        }

        public GameGroupMenuViewModel MapGameGroupMenu(IList<Group> availableGroups, Group selectedGroup)
        {
            return new GameGroupMenuViewModel()
            {
                CurrentGroup = selectedGroup.Name,
                AvailableGroupNames = availableGroups.Select(x => x.Name).ToList()
            };
        }


    }
    
}