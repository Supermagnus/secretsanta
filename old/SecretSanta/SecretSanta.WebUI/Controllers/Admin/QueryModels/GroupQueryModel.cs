﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretSanta.WebUI.Controllers.Admin.QueryModels
{
    public class GroupQueryModel
    {
        public GroupQueryModel()
        {
            AdminUserNames = new List<string>();
        }
        public int Id { get; set; }
        public string GroupName { get; set; }
        public IList<string> UserNames { get; set; }
        public IList<string> AdminUserNames { get; set; }

        public IList<string> BlacklistedUserPairs { get; set;} 
    }
}