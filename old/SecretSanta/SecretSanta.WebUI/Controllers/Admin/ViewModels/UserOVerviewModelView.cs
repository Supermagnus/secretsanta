﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Admin.ViewModels
{
    public class UserOVerviewModelView
    {
        public IList<User> Users { get; set; } 
    }
}