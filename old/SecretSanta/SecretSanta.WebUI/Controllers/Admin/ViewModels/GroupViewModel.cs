﻿using System;
using System.Collections.Generic;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Admin.ViewModels
{
    public class GroupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Which users that have administrative right for this group
        /// </summary>
        public IList<string> AdminUserNames { get; set; }
        public IList<string> UserNames { get; set; } 

        public DateTime? Started { get; set; }
        public IList<UserNamePair> BlacklistedPairs { get; set; }
        public IList<UserNamePair> GeneratedPairs { get; set; }
    }
}