﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SecretSanta.WebUI.Controllers.Admin
{
    public class UserNamePair
    {
        public string SecretSantaUserName { get; set; }
        public string  ReceiverUserName { get; set; }
    }
}