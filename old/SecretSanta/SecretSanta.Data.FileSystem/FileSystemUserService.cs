﻿//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.IO;
//using System.IO.Ports;
//using System.Text;
//using System.Xml.Linq;
//using SecretSanta.Core.ApplicationServices.Interfaces;
//using SecretSanta.Core.Domain;
//using SecretSanta.Data.FileSystem.Settings;

//namespace SecretSanta.Data.FileSystem
//{
//    public class FileSystemUserService : IUserService
//    {
//        private readonly string _userFolderPath;

//        private IDictionary<string,User> UserCache;
 
//        public FileSystemUserService(FileSystemSettings settings)
//        {
//            _userFolderPath = Path.Combine(settings.FolderPath, "Users");
         
//            UserCache = new Dictionary<string, User>();


//            if (!Directory.Exists(_userFolderPath))
//                Directory.CreateDirectory(_userFolderPath);
//        }
//        public int SaveUser(User user)
//        {
//            var path = Path.Combine(_userFolderPath, user.UserName + ".xml");
//            if (File.Exists(path))
//                File.Delete(path);
//            var xml = CreateUserXElement(user);
//            xml.Save(path);
//            return user.Id; //fix this
//        }

//        public User GetUser(string userName)
//        {
//            var path = Path.Combine(_userFolderPath, userName + ".xml");
//            if (!File.Exists(path))
//                return null;
//            var xRoot = XDocument.Load(path).Root;
//            var user = ParseUserXElement(xRoot);
//            return user;
//        }

//        public User GetUser(int userId)
//        {
//            throw new NotImplementedException();
//        }

//        public IList<User> GetAllUsers()
//        {
//            var users = new List<User>();
//            foreach (var userFile in Directory.GetFiles(_userFolderPath, "*.xml"))
//            {
//                users.Add(GetUser(userFile.Substring(0, userFile.Length - 4)));
//            }
//            return users;
//        }

//        public void DeleteUser(string userName)
//        {
//            var path = Path.Combine(_userFolderPath, userName + ".xml");
//            if (File.Exists(path))
//                File.Delete(path);
//        }

//        public void DeleteUser(int userId)
//        {
//            throw new NotImplementedException();
//        }

//        public void SaveUserMessage(UserMessage message, int fromUserId, int toUserId)
//        {
//            throw new NotImplementedException();
//        }

//        public IList<UserMessage> GetUserMessages(MessageFilter filter)
//        {
//            throw new NotImplementedException();
//        }

//        public int SaveWish(Wish wish, int userId)
//        {
//            throw new NotImplementedException();
//        }

//        public void DeleteWish(int wishId, int userId)
//        {
//            throw new NotImplementedException();
//        }

//        public IList<Wish> GetWishes(int userId)
//        {
//            throw new NotImplementedException();
//        }


//        private XElement CreateUserXElement(User user)
//        {
//            var xWishlist = new XElement("Wishlist");
//            foreach (var wish in user.Wishlist)
//            {
//                var xWish = new XElement("Wish", wish.Description);
//                xWish.Add(new XAttribute("Id", wish.Id));
//                xWishlist.Add(xWish);
//            }
//            var xInbox = new XElement("Inbox");
//            //foreach (var message in user.Inbox)
//            //{
//            //    var xMessage = new XElement("Message", message.Message);
//            //    xMessage.Add(new XAttribute("CreateDate", message.CreateDate.ToString()));
//            //    xMessage.Add(new XAttribute("FromUserName", message.UserName));
//            //    xInbox.Add(xMessage);
//            //}
//            var xRoot = new XElement("User",
//                new XAttribute("UserName", user.UserName ?? ""),
//                new XElement("FirstName", user.FirstName ?? ""),
//                new XElement("LastName", user.LastName ?? ""),
//                new XElement("Email", user.Email ?? ""),
//                new XElement("Password", user.Password ?? ""),
//                new XElement("LastLogin", user.LastLoginDate.HasValue 
//                    ? user.LastLoginDate.Value.ToString(CultureInfo.InvariantCulture)
//                    : ""
//                    ),
//                    xWishlist,
//                    xInbox);

//            return xRoot;

//        }

//        private User ParseUserXElement(XElement xRoot)
//        {
           
//            var user =  new User()
//            {
//                UserName = xRoot.FirstAttribute.Value,
//                FirstName = xRoot.Element("FirstName").Value,
//                LastName = xRoot.Element("LastName").Value,
//                Email = xRoot.Element("Email").Value,
//                Password = xRoot.Element("Password").Value,
//                Wishlist = ParseWishlist(xRoot),
//                //Inbox = ParseInbox(xRoot)
//            };
//            DateTime date;
//            if (DateTime.TryParse(xRoot.Element("LastLogin").Value, out date))
//                user.LastLoginDate = date;
//            else
//                user.LastLoginDate = null;
//            return user;

//        }

//        private IList<Wish> ParseWishlist(XElement xRoot)
//        {
//            var wishlist = new List<Wish>();
//            var xWishlist = xRoot.Element("Wishlist");
//            if (xWishlist != null)
//            {
//                foreach (var xWish in xWishlist.Elements())
//                {
//                    wishlist.Add(new Wish()
//                    {
//                        Id = int.Parse(xWish.FirstAttribute.Value),
//                        Description = xWish.Value
//                    });
//                }
//            }
//            return wishlist;
//        }

//        private IList<UserMessage> ParseInbox(XElement xRoot)
//        {
//            var inbox = new List<UserMessage>();
//            var xInbox = xRoot.Element("Inbox");
//            if (xInbox != null)
//            {
//                foreach (var xMessage in xInbox.Elements())
//                {
//                    inbox.Add(new UserMessage()
//                    {
//                        CreateDate= DateTime.Parse(xMessage.Attribute("CreateDate").Value),
//                        UserName= xMessage.Attribute("FromUserName").Value,
//                        Message =  xMessage.Value
//                    });
//                }
//            }
//            return inbox;
//        } 
//    }
//}
