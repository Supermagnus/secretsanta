﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Xml.Linq;
//using SecretSanta.Core.ApplicationServices.Implementations;
//using SecretSanta.Core.ApplicationServices.Interfaces;
//using SecretSanta.Core.Domain;
//using SecretSanta.Data.FileSystem.Settings;

//namespace SecretSanta.Data.FileSystem
//{
//    public class FileSystemGroupService : GroupServiceBase, IGroupService
//    {

//        private readonly string _groupFolderPath;
//        private IDictionary<string, Group> GroupCache;

//        public FileSystemGroupService(FileSystemSettings settings)
//        {
//            _groupFolderPath = Path.Combine(settings.FolderPath, "Groups");
//            GroupCache = new Dictionary<string, Group>();


//            if (!Directory.Exists(_groupFolderPath))
//                Directory.CreateDirectory(_groupFolderPath);
//        }


//        public int SaveGroup(Group group)
//        {
//            var path = Path.Combine(_groupFolderPath, group.Name + ".xml");
//            if (File.Exists(path))
//                File.Delete(path);
//            var xml = CreateGroupXElement(group);
//            xml.Save(path);
//            return group.Id; //todo fix if youwant to use this implementation
//        }

//        public Group GetGroup(string groupName)
//        {
//            var path = Path.Combine(_groupFolderPath, groupName + ".xml");
//            if (!File.Exists(path))
//                return null;
//            var xRoot = XDocument.Load(path).Root;
//            var group = ParseGroupXElement(xRoot);


//            return group;

//        }

//        public Group GetGroup(int groupId)
//        {
//            throw new NotImplementedException();
//        }

//        public IList<Group> GetAllGroups()
//        {
//            var groups = new List<Group>();
//            foreach (var groupFile in Directory.GetFiles(_groupFolderPath, "*.xml"))
//            {
//                groups.Add(GetGroup(groupFile.Substring(0, groupFile.Length - 4)));
//            }
//            return groups;
//        }

//        public IList<Group> GetGroupsForUser(string userName)
//        {
//            return GetAllGroups()
//                .Where(group => group.Users.Any(x=>x.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase)))
//                .ToList();
//        }

//        public IList<Group> GetGroupsForUser(int userId)
//        {
//            throw new NotImplementedException();
//        }

//        public void DeleteGroup(string groupName)
//        {
//            var path = Path.Combine(_groupFolderPath, groupName + ".xml");
//            if (File.Exists(path))
//                File.Delete(path);
//        }

//        public void DeleteGroup(int groupId)
//        {
//            throw new NotImplementedException();
//        }

//        public IList<Group> GetAdministratedGroups(int usedId)
//        {
//            throw new NotImplementedException();
//        }

//        public void SaveGroupMessage(UserMessage message, int fromUserId, int toGroupId)
//        {
//            throw new NotImplementedException();
//        }

//        public IList<UserMessage> GetGroupMessages(int groupId, int maxMessages)
//        {
//            throw new NotImplementedException();
//        }


//        private XElement CreateGroupXElement(Group group)
//        {
//            XElement root = new XElement("Group");
//            root.Add(new XAttribute("Name", group.Name));
//            root.Add(new XElement("Started", group.Started.HasValue ? group.Started.Value.ToString() : ""));
//            var xUsers = new XElement("Users");
//            foreach (var user in group.Users)
//            {
//                xUsers.Add(new XElement("UserId", user.UserName));
//            }
//            var xBlackListedPairs = new XElement("BlackListedPairs");
//            foreach (var pair in group.BlacklistedPairs)
//            {
//                var xPair = new XElement("Pair");
//                xPair.Add(new XAttribute("SecretSanta", pair.SecretSantaId));
//                xPair.Add(new XAttribute("Receiver", pair.ReceiverId));
//                xBlackListedPairs.Add(xPair);
//            }

//            var xPairs = new XElement("GeneratedPairs");
//            foreach (var pair in group.GeneratedPairs)
//            {
//                var xPair = new XElement("Pair");
//                xPair.Add(new XAttribute("SecretSanta", pair.SecretSantaId));
//                xPair.Add(new XAttribute("Receiver", pair.ReceiverId));
//                xPairs.Add(xPair);
//            }
//            var xMessages = new XElement("Messages");
//            foreach (var userMessage in group.Messages)
//            {
//                var xMsg = new XElement("Message", userMessage.Message);
//                xMsg.Add(new XAttribute("CreateDate", userMessage.CreateDate.ToString()),
//                    new XAttribute("UserName", userMessage.UserName));
//                xMessages.Add(xMsg);
//            }

//            root.Add(xUsers, xPairs, xBlackListedPairs, xMessages);
            
//            return root;
//        }

//        private Group ParseGroupXElement(XElement xRoot)
//        {
//            var name = xRoot.FirstAttribute.Value;
//            var started = xRoot.Element("Started").Value;
//            var xUsers = xRoot.Element("Users");
            
//            var xgeneratedPairs = xRoot.Element("GeneratedPairs");

//            var users = xUsers.Elements("UserId").Select(x => new User() { UserName = x.Value });
//            var generatedPairs = xgeneratedPairs.Elements("Pair").Select(x => new UserPair()
//            {
//                SecretSantaId = x.Attribute("SecretSanta").Value
//                ,
//                ReceiverId = x.Attribute("Receiver").Value,

//            });

//            var xBlackListedPairs = xRoot.Element("BlackListedPairs");
//            var blackListedPairs = xBlackListedPairs.Elements("Pair").Select(x => new UserPair()
//            {
//                SecretSantaId = x.Attribute("SecretSanta").Value
//                ,
//                ReceiverId = x.Attribute("Receiver").Value,

//            });

//            var xMessages = xRoot.Element("Messages");
//            IEnumerable<UserMessage> messages = new UserMessage[0];
//            if(xMessages != null)
//                messages = xMessages.Elements("Message").Select(x => new UserMessage()
//                {
//                    Message = x.Value,
//                    CreateDate = DateTime.Parse(x.Attribute("CreateDate").Value),
//                    UserName = x.Attribute("UserName").Value
//                });
//            return new Group()
//            {
//                Name = name,
//                Users = users.ToList(),
//                GeneratedPairs = generatedPairs.ToList(),
//                BlacklistedPairs = blackListedPairs.ToList(),
//                Messages = messages.ToList(),
//                Started = string.IsNullOrEmpty(started) ? (DateTime?) null : DateTime.Parse(started)
//            };
//        }



//    }
//}