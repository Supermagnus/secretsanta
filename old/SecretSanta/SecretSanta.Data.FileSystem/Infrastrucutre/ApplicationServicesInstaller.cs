using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using SecretSanta.Core.ApplicationServices.Implementations;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Data.FileSystem.Settings;

namespace SecretSanta.Data.FileSystem.Infrastrucutre
{
    public class FileSystemInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {


            container.Register(
                Component.For<FileSystemSettings>()
                    .ImplementedBy<FileSystemSettings>()
                );

            //container.Register(
            //    Component.For<IUserService>()
            //        .ImplementedBy<FileSystemUserService>(),
            //    Component.For<IGroupService>()
            //    .ImplementedBy<FileSystemGroupService>()
                
            //    );


        }
    }
}