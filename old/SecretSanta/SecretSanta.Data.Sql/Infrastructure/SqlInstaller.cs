using Castle.MicroKernel.Registration;
using Castle.Windsor;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Data.Sql.Databases;
using SecretSanta.Data.Sql.Databases.SecretSanta.Mappers;
using SecretSanta.Data.Sql.Settings;

namespace SecretSanta.Data.Sql.Infrastructure
{
    public class SqlInstaller : IWindsorInstaller
    {


        public void Install(IWindsorContainer container,
                            Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {

            var forceExtension = DapperExtensions.Operator.Eq;

            RegisterServicesTo(container);

            container.Register(
                Component.For<SqlSettings>()
                .ImplementedBy<SqlSettings>()
                .LifestyleSingleton());
        }

        private void RegisterServicesTo(IWindsorContainer container)
        {

            container.Register(
                Component.For<IGroupService>()
                .ImplementedBy<SqlGroupService>()
                .LifestyleSingleton());

            container.Register(
                Component.For<IUserService>()
                .ImplementedBy<SqlUserService>()
                .LifestyleSingleton());

            container.Register(
              Component.For<SecretSantaDatabase>()
              .ImplementedBy<SecretSantaDatabase>()
                  .LifestyleSingleton());
        }


     


    }






}