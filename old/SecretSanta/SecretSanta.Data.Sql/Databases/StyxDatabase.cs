using System.Collections.Generic;
using SecretSanta.Data.Sql.Databases.Common;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;
using SecretSanta.Data.Sql.Settings;

namespace SecretSanta.Data.Sql.Databases
{




    public class SecretSantaDatabase : DatabaseBase
    {
        public SecretSantaDatabase(SqlSettings settings)
            : base(settings.SecretSantaConnectionString)
        {

            DbObjects = new List<IDatabaseObject>()
                                  {
                              new UserMessageTable(),
                              new UserTable(),
                              new WishTable(),
                              new GroupMessageTable(),
                              new GroupTable(),
                              new GroupUserTable()
                          };


        }


    }
}