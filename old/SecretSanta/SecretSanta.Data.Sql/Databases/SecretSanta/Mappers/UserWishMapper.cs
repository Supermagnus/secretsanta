using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Mappers
{
    public static class WishMapper
    {
        public static WishModel Map(Wish sourceObject, int userId)
        {
            return new WishModel()
            {
                Id = sourceObject.Id,
                Description = sourceObject.Description,
                UserId = userId
            };
        }

        public static Wish MapBack(WishModel sourceObject)
        {
            return new Wish()
            {
                Description = sourceObject.Description,
                Id = sourceObject.Id
            };
        }
    }
}