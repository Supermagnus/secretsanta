using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Mappers
{
    public static class GroupMapper
    {
        public static GroupModel Map(Group sourceObject)
        {
            return new GroupModel()
            {
                Id = sourceObject.Id,
                Name = sourceObject.Name,
                GeneratedPairs = MapToString(sourceObject.GeneratedPairs),
                BlacklistedPairs = MapToString(sourceObject.BlacklistedPairs),
                Started = sourceObject.Started
            };
        }

        public static Group MapBack(GroupModel sourceObject, IList<GroupUserModel> users)
        {
            return new Group()
            {
                Name = sourceObject.Name,
                GeneratedPairs = MapFromString(sourceObject.GeneratedPairs),
                BlacklistedPairs = MapFromString(sourceObject.BlacklistedPairs),
                Started = sourceObject.Started,
                UserIds = users.Where(x=>!x.IsAdmin).Select(x=>x.UserId).ToList(),
                AdminUserIds = users.Where(x=> x.IsAdmin).Select(x=>x.UserId).ToList(),
                Id =  sourceObject.Id
            };
        }

        private const char PairDelimiter = ';';
        private const string RelationDelimiter = "->";
        public static IList<UserPair> MapFromString(string userPairs)
        {
            var pairs = new List<UserPair>();
            foreach (var s in userPairs.Split(PairDelimiter))
            {
                var tokens = s.Split(new []{RelationDelimiter}, StringSplitOptions.RemoveEmptyEntries);
                if(tokens.Length != 2)
                    continue;
                pairs.Add(new UserPair()
                {
                    SecretSantaId = int.Parse(tokens[0]),
                    ReceiverId = int.Parse(tokens[1])
                });
            }
            return pairs;
        }

        public static string MapToString(IList<UserPair> userPairs)
        {
            return string.Join(PairDelimiter.ToString(),
                userPairs.Select(x => string.Format("{0}{1}{2}",
                    x.SecretSantaId, RelationDelimiter, x.ReceiverId)));
        }
    }

}