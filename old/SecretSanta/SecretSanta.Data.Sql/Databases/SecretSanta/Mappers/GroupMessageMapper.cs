using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Mappers
{
    public static class GroupMessageMapper
    {

        public static GroupMessageModel Map(UserMessage sourceObject, int fromUserId, int toGroupId)
        {
            return new GroupMessageModel()
            {
                CreateDate = sourceObject.CreateDate,
                FromUserId = fromUserId,
                ToGroupId = toGroupId,
                Message = sourceObject.Message
            };
        }

        //public static UserMessage MapBack(UserMessageModel sourceObject)
        //{
        //    return new UserMessage()
        //    {
        //        CreateDate = sourceObject.CreateDate,
        //        Message = sourceObject.Message,
        //        UserName = sourceObject.FromUserName

        //    };
        //}
    }
}