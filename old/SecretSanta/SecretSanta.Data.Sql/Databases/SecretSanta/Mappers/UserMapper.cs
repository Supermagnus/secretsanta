using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases.Common;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Mappers
{
    public static class UserMapper 
    {
        public static UserModel Map(User sourceObject)
        {
            return new UserModel()
            {
                Email = sourceObject.Email,
                UserName = sourceObject.UserName,
                FirstName = sourceObject.FirstName,
                LastLoginDate = sourceObject.LastLoginDate,
                LastName = sourceObject.LastName,
                Password = sourceObject.Password,
                Id = sourceObject.Id
            };
        }

        public static User MapBack(UserModel sourceObject )
        {
            return new User()
            {
                UserName = sourceObject.UserName,
                FirstName = sourceObject.FirstName,
                LastName = sourceObject.LastName,
                Email = sourceObject.Email,
                LastLoginDate = sourceObject.LastLoginDate,
                Password = sourceObject.Password,
                Id = sourceObject.Id
            };
        }
    }
}