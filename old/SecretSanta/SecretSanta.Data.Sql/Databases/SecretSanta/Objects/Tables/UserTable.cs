using SecretSanta.Data.Sql.Databases.Common;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables
{
    internal class UserTable : TableBase, IDatabaseObject
    {

        public static string TableName
        {
            get { return "Users"; }
        }

        protected override string GetConstraintsSql()
        {
            return "";
        }

        protected override string GetCreateTableSql()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, " +
                                 "UserName nvarchar(256) NOT NULL UNIQUE, " +
                                 "FirstName nvarchar(256) , " +
                                 "LastName nvarchar(256) , " +
                                 "Email nvarchar(256), " +
                                 "LastLoginDate date," +
                                 "Password nvarchar(256))"
                                 , TableName);

        }


        protected override string GetTableName()
        {
            return TableName;
        }
    }
}