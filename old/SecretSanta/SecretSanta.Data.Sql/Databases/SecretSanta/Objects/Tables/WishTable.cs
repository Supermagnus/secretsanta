using SecretSanta.Data.Sql.Databases.Common;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables
{
    internal class  WishTable : TableBase, IDatabaseObject
    {

        public static string TableName
        {
            get { return "UserWishes"; }
        }

        protected override string GetConstraintsSql()
        {
            return
                CreateForeignKeySql(
                    UserTable.TableName, "Id",
                    this.GetTableName(), "UserId");
        }

        protected override string GetCreateTableSql()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, " +
                                 "UserId int NOT NULL, " +
                                 "Description nvarchar(1024))" 
                                 , TableName);

        }


        protected override string GetTableName()
        {
            return TableName;
        }
    }
}