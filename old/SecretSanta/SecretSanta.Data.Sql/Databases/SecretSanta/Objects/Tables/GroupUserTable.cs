using SecretSanta.Data.Sql.Databases.Common;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables
{
    internal class GroupUserTable : TableBase, IDatabaseObject
    {

        public static string TableName
        {
            get { return "GroupUsers"; }
        }

        protected override string GetConstraintsSql()
        {
            return
         CreateForeignKeySql(
             UserTable.TableName, "Id",
             this.GetTableName(), "UserId")
             + " " +
             CreateForeignKeySql(
             GroupTable.TableName, "Id",
             this.GetTableName(), "GroupId");

        }

        protected override string GetCreateTableSql()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "GroupId int NOT NULL, " +
                                 "UserId int NOT NULL," +
                                 "IsAdmin bit NOT NULL)"
                , TableName);

        }


        protected override string GetTableName()
        {
            return TableName;
        }
    }
}