using DapperExtensions.Mapper;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class WishModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }

    }
    public class UserWishModelAutoMapper : ClassMapper<WishModel>
    {
        public UserWishModelAutoMapper()
        {
            TableName = WishTable.TableName;
            AutoMap();
        }
    }
}