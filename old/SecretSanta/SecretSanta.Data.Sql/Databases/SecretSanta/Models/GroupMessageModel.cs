using System;
using DapperExtensions.Mapper;
using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class GroupMessageModel
    {
        public int Id { get; set; }
        public int ToGroupId { get; set; }
        public int FromUserId { get; set; }

        public DateTime CreateDate { get; set; }
        public string Message{ get; set; }

    }

    public class GroupMessageModelAutoMapper : ClassMapper<GroupMessageModel>
    {
        public GroupMessageModelAutoMapper()
        {
            TableName = GroupMessageTable.TableName;
            AutoMap();
        }
    }
}