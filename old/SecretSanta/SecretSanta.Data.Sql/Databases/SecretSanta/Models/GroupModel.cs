using System;
using System.Collections;
using System.Collections.Generic;
using DapperExtensions.Mapper;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class GroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Started { get; set; }
        public string BlacklistedPairs { get; set; }
        public string GeneratedPairs { get; set; }
        
    }

    public class GroupModelAutoMapper : ClassMapper<GroupModel>
    {
        public GroupModelAutoMapper()
        {
            TableName = GroupTable.TableName;
            AutoMap();
        }
    }

  
}