﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DapperExtensions.Mapper;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class GroupUserModel
    {
        public int GroupId { get; set; }
        public int UserId { get; set; }
        public bool IsAdmin { get; set; }

    }

    public class GroupUserModellAutoMapper : ClassMapper<GroupUserModel>
    {
        public GroupUserModellAutoMapper()
        {
            TableName = GroupUserTable.TableName;

            Map(x => x.GroupId);
            Map(x => x.UserId);
            Map(x => x.IsAdmin);
        }
    }
}
