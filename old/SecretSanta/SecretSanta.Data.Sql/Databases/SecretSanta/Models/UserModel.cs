using System;
using System.Collections;
using System.Collections.Generic;
using DapperExtensions.Mapper;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName{ get; set; }

        public string Email{ get; set; }

        public DateTime? LastLoginDate { get; set; }

        public string Password { get; set; }
    }

    public class UserModelAutoMapper : ClassMapper<UserModel>
    {
        public UserModelAutoMapper()
        {
            TableName = UserTable.TableName;
            AutoMap();
        }
    }
}