using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace SecretSanta.Data.Sql.Databases.Common
{
    public abstract class TableBase
    {
        protected abstract string GetConstraintsSql();
        protected abstract string GetCreateTableSql();
        protected abstract string GetTableName();

        public bool Exists(SqlConnection connection)
        {
            OpenConnectionIfClosed(connection);
            var count = connection.Query<int>(
                string.Format("(SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{0}')",
                              GetTableName())).FirstOrDefault();
            return count > 0;
        }

        public void Create(SqlConnection connection)
        {
            OpenConnectionIfClosed(connection);
            if (connection.State != ConnectionState.Open)
                connection.Open();
            connection.Execute(GetCreateTableSql());
        }

        public void SetConstraints(SqlConnection connection)
        {
            string sql = GetConstraintsSql();
            if(string.IsNullOrEmpty(sql))
                return;
            
            OpenConnectionIfClosed(connection);
            if (connection.State != ConnectionState.Open)
                connection.Open();
            connection.Execute(sql);
        }

        
        

        public void Drop(SqlConnection connection)
        {
            OpenConnectionIfClosed(connection);
            connection.Execute(string.Format("DROP TABLE {0}", GetTableName()));
        }

        private void OpenConnectionIfClosed(SqlConnection conn)
        {
            if(conn.State != ConnectionState.Open)
                conn.Open();
        }


        protected string CreateForeignKeySql(
            string primaryKeyTable, string primaryKeyTableColumn,
            string foreignKeyTable, string foreignKeyTableColumn)
        {
            return 
                string.Format("ALTER TABLE [dbo].[{2}]  WITH CHECK ADD  CONSTRAINT [FK_{2}_{0}_{3}] FOREIGN KEY([{3}]) REFERENCES [dbo].[{0}] ([{1}]) " +
                              "ALTER TABLE [dbo].[{2}] CHECK CONSTRAINT [FK_{2}_{0}_{3}]",
                    primaryKeyTable,
                    primaryKeyTableColumn,
                    foreignKeyTable,
                    foreignKeyTableColumn
                );
        }
    }
}