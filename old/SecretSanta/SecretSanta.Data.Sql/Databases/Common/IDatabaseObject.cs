﻿using System.Data.SqlClient;

namespace SecretSanta.Data.Sql.Databases.Common
{
    public interface IDatabaseObject
    {
        bool Exists(SqlConnection connection);
        void Create(SqlConnection connection);
        void SetConstraints(SqlConnection connection);
        void Drop(SqlConnection connection);
    }
}
