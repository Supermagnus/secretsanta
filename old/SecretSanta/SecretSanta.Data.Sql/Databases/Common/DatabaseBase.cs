using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace SecretSanta.Data.Sql.Databases.Common
{
    public abstract class DatabaseBase
    {
        private readonly string _connectionString;
        protected IList<IDatabaseObject> DbObjects;

        protected DatabaseBase(string connectionString)
        {
            this._connectionString = connectionString;
            DbObjects = new List<IDatabaseObject>();
        }


        /// <summary>
        /// Verifies the integrity of the database
        /// Creates tables that does not exist
        /// </summary>
        /// <param name="conn"></param>
        public void Setup(SqlConnection conn)
        {
            var createdObjects = new List<IDatabaseObject>();
            foreach (var dbOject in DbObjects.Where(dbOject => !dbOject.Exists(conn)))
            {
                dbOject.Create(conn);
                createdObjects.Add(dbOject);
            }
            foreach (var dbOject in createdObjects)
            {
                dbOject.SetConstraints(conn);
            }
        }

        public SqlConnection GetOpenConnection()
        {
            var connection = new SqlConnection(_connectionString);
            connection.Open();
            return connection;
        }
    }
}