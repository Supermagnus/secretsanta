﻿namespace SecretSanta.Data.Sql.Databases.Common
{
    public interface IMapper<TFrom,TTo>
    {
        TTo Map(TFrom sourceObject);
        TFrom MapBack(TTo sourceObject);
    }
}
