﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SecretSanta.Data.Sql.Settings
{
   public  class SqlSettings
    {
       public SqlSettings()
       {
           Initialize();

       }
       public string SecretSantaConnectionString { get; set; }

       private void Initialize()
       {
           var connection = ConfigurationManager.ConnectionStrings["SecretSantaConnectionString"];
           if(connection != null)
               SecretSantaConnectionString = ConfigurationManager.ConnectionStrings["SecretSantaConnectionString"].ConnectionString;
       }
    }
    
}
