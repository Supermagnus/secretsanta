using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using DapperExtensions;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases;
using SecretSanta.Data.Sql.Databases.SecretSanta.Mappers;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql
{
    public class SqlUserService : IUserService
    {
        private readonly SecretSantaDatabase _santaDatabase;


        public SqlUserService(SecretSantaDatabase santaDatabase)
        {
            _santaDatabase = santaDatabase;

            using (var conn = _santaDatabase.GetOpenConnection())
            {
                _santaDatabase.Setup(conn);
            }
        }


        public int  SaveUser(User user)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var userModel = UserMapper.Map(user);

                var id = user.Id;
                if (!UserExists(user.Id, connection,transaction))
                {
                    
                  id = connection.Insert(userModel, transaction);

                }
                else
                {
                    connection.Update(userModel, transaction);
                }

                transaction.Commit();
                user.Id = id;
                return id;
            }
        
        }

        
        public User GetUser(string userName)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var user = connection.Query<UserModel>(string.Format("SELECT * FROM {0} WHERE UserName = @userName", UserTable.TableName),
                    new { userName = userName})
                    .FirstOrDefault();
                return user == null ? null : UserMapper.MapBack(user);
            }
           
        }

        public User GetUser(int userId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var user = connection.Query<UserModel>(string.Format("SELECT * FROM {0} WHERE id = @id", UserTable.TableName),
                    new { id = userId })
                    .FirstOrDefault();
                return user == null ? null : UserMapper.MapBack(user);
            }

        }

        public IList<User> GetAllUsers()
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var users = connection.Query<UserModel>(string.Format("SELECT * FROM {0}", UserTable.TableName));
                return users.Select(UserMapper.MapBack).ToList();
            }
        }

        public void DeleteUser(string userName)
        {
             using (var connection = _santaDatabase.GetOpenConnection())
             using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
             {
                 connection.Execute("DELETE FROM " + UserTable.TableName + " WHERE UserName = @userName",
                     new {  userName}, transaction);
                 transaction.Commit();

             }
        }

        public void DeleteUser(int userId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                connection.Execute("DELETE FROM " + UserTable.TableName + " WHERE Id = @id",
                    new { id = userId}, transaction);
                transaction.Commit();

            }
        }

        public void SaveUserMessage(UserMessage message, int fromUserId, int toUserId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var model = UserMessageMapper.Map(message, fromUserId,toUserId);
                connection.Insert(model);
            }

        }

        public IList<UserMessage> GetUserMessages(params MessageFilter[] filters)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var sql = string.Format("SELECT TOP {0} u.UserName, m.CreateDate, m.[Message]  FROM {1} m JOIN {2} u ON u.Id = m.FromUserId {3} ORDER BY m.CreateDate desc",
                     filters.Max(x=>x.MaxMessages),
                    UserMessageTable.TableName,
                    UserTable.TableName,
                    GetMessageFilterConstraint(filters));

                var messages = connection.Query<UserMessage>(sql);
                return messages.ToList();
            }
        }

        public int SaveWish(Wish wish, int userId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var wishModel = WishMapper.Map(wish,userId);

                var id = wish.Id;
                if (!WishExists(id, connection, transaction))
                {
                    id = connection.Insert(wishModel, transaction);

                }
                else
                {
                    connection.Update(wishModel, transaction);
                }
                transaction.Commit();
                return id;
            }
        
        }

        public void DeleteWish(int wishId, int userId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                connection.Execute("DELETE FROM " + WishTable.TableName + " WHERE Id = @id AND UserId = @userId",
                    new { id = wishId, userId = userId }, transaction);
                transaction.Commit();

            }
        }

        public IList<Wish> GetWishes(int userId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var sql = string.Format("SELECT * FROM {0} WHERE userId = @userId",
                     WishTable.TableName);

                var wishes = connection.Query<WishModel>(sql, new {userId = userId});
                return wishes.Select(WishMapper.MapBack).ToList();
            }
        }

        private string GetMessageFilterConstraint(params MessageFilter[] filters)
        {
            StringBuilder constraint = new StringBuilder();
            constraint.Append(" WHERE ");
            
            foreach (var filter in filters)
            {
                if (!filter.FromUserId.HasValue && !filter.ToUserId.HasValue)
                    continue;

                constraint.Append("(");
            
                if (filter.FromUserId.HasValue)
                    constraint.Append(" FromUserId = " + filter.FromUserId.Value);
                if (filter.ToUserId.HasValue)
                    constraint.Append(" AND ");
                if(filter.ToUserId.HasValue)
                    constraint.Append(" ToUserId = " + filter.ToUserId.Value);

                constraint.Append(")");
                constraint.Append(" OR ");
            }
            return constraint.ToString().Substring(0,constraint.Length-4);
        }

        private bool UserExists(int userId, SqlConnection connection, SqlTransaction transaction)
        {
            return connection.Query<int>(string.Format("SELECT COUNT(*) FROM {0} WHERE id = @id", UserTable.TableName),
                    new { id = userId}, transaction)
                    .FirstOrDefault() > 0;
        }
        private bool WishExists(int wishId, SqlConnection connection, SqlTransaction transaction)
        {
            return connection.Query<int>(string.Format("SELECT COUNT(*) FROM {0} WHERE id = @id", WishTable.TableName),
                    new { id = wishId }, transaction)
                    .FirstOrDefault() > 0;
        }
    }
}