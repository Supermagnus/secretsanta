﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Game.Domain
{
    public class LinkedUsers
    {
        public User LoggedInUser { get; set; }
        
        public User SecretSantaUser { get; set; }

        /// <summary>
        /// THe conversation between the user and his SecretSanta
        /// </summary>
        public IList<Message> SecretSantaConversation { get; set; }
 
        public User ReceiverUser { get; set; }
        /// <summary>
        /// The conversation between the logged in user and the user hes giving to 
        /// </summary>
        public IList<Message> ReceivingUserConversation { get; set; }
    }
}