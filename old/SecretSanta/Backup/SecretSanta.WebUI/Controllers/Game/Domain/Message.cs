﻿using System;

namespace SecretSanta.WebUI.Controllers.Game.Domain
{
    public class Message
    {
        public string Sender { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set; }
    }
}