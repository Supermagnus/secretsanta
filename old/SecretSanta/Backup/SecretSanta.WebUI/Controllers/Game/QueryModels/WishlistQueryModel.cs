﻿using System.Collections.Generic;

namespace SecretSanta.WebUI.Controllers.Game.QueryModels
{
    public class WishQueryModel :QueryModelBase
    {
        public string Wish { get; set; }
        public int WishId { get; set; }
    }
}