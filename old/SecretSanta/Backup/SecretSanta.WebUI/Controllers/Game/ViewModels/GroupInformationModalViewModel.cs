﻿using System.Collections;
using System.Collections.Generic;
using Castle.MicroKernel;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class GroupInformationModalViewModel
    {
        public Group Group { get; set; }
        public string ModalId { get; set; }
        public IList<User> Users { get; set; }
    }
}