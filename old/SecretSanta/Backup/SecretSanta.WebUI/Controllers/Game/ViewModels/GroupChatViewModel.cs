﻿using System.Collections.Generic;
using System.Web.UI.WebControls.Expressions;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class GroupChatViewModel : GameBaseViewModel
    {
        public IList<UserMessage> Messages { get; set; }
        public string SendingUserName { get; set; }
        public string GroupName { get; set; }
        public int GroupId { get; set; }
    }
}