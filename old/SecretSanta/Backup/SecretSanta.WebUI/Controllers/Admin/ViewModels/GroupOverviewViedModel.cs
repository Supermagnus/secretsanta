﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using SecretSanta.Core.Domain;

namespace SecretSanta.WebUI.Controllers.Admin.ViewModels
{
    public class GroupOverviewViedModel
    {
        public IList<GroupViewModel> GroupViewModels { get; set; }

    }
}