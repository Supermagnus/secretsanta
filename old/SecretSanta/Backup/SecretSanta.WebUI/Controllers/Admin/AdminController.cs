﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Castle.MicroKernel.SubSystems.Conversion;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.WebUI.Controllers.Admin.QueryModels;
using SecretSanta.WebUI.Controllers.Admin.ViewModels;

namespace SecretSanta.WebUI.Controllers.Admin
{
    public class AdminController : Controller
    {
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private readonly IEmailService _emailService;

        public AdminController(IUserService userService, IGroupService groupService, IEmailService emailService)
        {
            _userService = userService;
            _groupService = groupService;
            _emailService = emailService;
        }

        public ActionResult Index()
        {
            return UserOverview();
        }

        public ActionResult UserOverview()
        {
            var model = new UserOVerviewModelView
            {
                Users = _userService.GetAllUsers()
            };
            return View("UserOVerview",model);
        }

        public ActionResult GroupOverview()
        {
            var groups = _groupService.GetAllGroups();
            var model = new GroupOverviewViedModel
            {
                GroupViewModels = groups.Select(group=> new GroupViewModel()
                {
                    Name = group.Name,
                    UserNames = group.UserIds.Select(x => _userService.GetUser(x).UserName).ToList(),
                    AdminUserNames = group.AdminUserIds.Select(x=>_userService.GetUser(x).UserName).ToList(),
                    BlacklistedPairs = group.BlacklistedPairs.Select(UserPairToUserNamePair).ToList(),
                    GeneratedPairs = group.GeneratedPairs.Select(UserPairToUserNamePair).ToList(),
                    Id = group.Id,
                    Started = group.Started
                }).ToList()
            };
            return View("GroupOverview", model);
        }


        [HttpPost]
        public JsonResult SaveOrUpdateUser(User user)
        {
            _userService.SaveUser(user);
            return Json("ok");
        }

        [HttpPost]
        public ActionResult DeleteUser(int userId)
        {
            _userService.DeleteUser(userId);
            return RedirectToAction("UserOverview");
        }

        public ActionResult ClearUserInbox(int userId)
        {
            throw new NotSupportedException();
            return RedirectToAction("UserOverview");
        }

        public ActionResult AddNewUser()
        {
            var user = new User();
            _userService.SaveUser(user);
            return RedirectToAction("UserOverview");

        }


        [HttpPost]
        public JsonResult SaveOrUpdateGroup(GroupQueryModel groupQueryModel)
        {
            var inputUsers = groupQueryModel.UserNames.SelectMany(x => x.Split(new char[] {',', ';'})).ToList();
            var inputAdmins= groupQueryModel.AdminUserNames.SelectMany(x => x.Split(new char[] { ',', ';' })).ToList();

            var users = inputUsers.Select(x => _userService.GetUser(x.Trim())).Where(x => x != null);
            var admins = inputAdmins.Select(x => _userService.GetUser(x.Trim())).Where(x => x != null);
            var blackList = GenerateBlacklist(groupQueryModel).ToList();
            var group = new Group()
            {
                Name = groupQueryModel.GroupName,
                UserIds = users.Select(x=>x.Id).ToList(),
                AdminUserIds = admins.Select(x=>x.Id).ToList(),
                BlacklistedPairs = blackList.Select(UserNamePairToUserPair).ToList(),
                Id = groupQueryModel.Id
            };
            _groupService.SaveGroup(group);
            return Json("ok");
        }

        [HttpPost]
        public ActionResult DeleteGroup(int groupId)
        {
            _groupService.DeleteGroup(groupId);
           return RedirectToAction("GroupOverview");

        }

        public ActionResult AddNewGroup()
        {
           
            _groupService.SaveGroup(new Group()); 
            return RedirectToAction("GroupOverview");

        }

        public ActionResult StartGroup(int groupId)
        {
            var group = _groupService.GetGroup(groupId);
            if (group == null)
                return GroupOverview();
            group.GeneratedPairs = _groupService.GenerateUserPairs(group);
            group.Started = DateTime.Now;
            _groupService.SaveGroup(group);

            var gameUrl = Request.Url.GetLeftPart(UriPartial.Authority);
            //_emailService.SendLoginEmails(group, group.UserIds.Select(id => _userService.GetUser(id)).ToList(), gameUrl);
            return RedirectToAction("GroupOverview");
        }

        //Fugly, fix
        private UserNamePair UserPairToUserNamePair(UserPair userpair)
        {
            return new UserNamePair()
            {
                ReceiverUserName = _userService.GetUser(userpair.ReceiverId).UserName,
                SecretSantaUserName = _userService.GetUser(userpair.SecretSantaId).UserName
            };
        }
        private UserPair UserNamePairToUserPair(UserNamePair usernamePair)
        {
            return new UserPair()
            {
                ReceiverId = _userService.GetUser(usernamePair.ReceiverUserName).Id,
                SecretSantaId = _userService.GetUser(usernamePair.SecretSantaUserName).Id

            };
        }

        private IEnumerable<UserNamePair> GenerateBlacklist(GroupQueryModel groupQueryModel)
        {
             var blackListPairStrings = groupQueryModel.BlacklistedUserPairs.SelectMany(x => x.Split(new char[] {','})).ToList();
            foreach (var blackListPairString in blackListPairStrings)
            {
                var mutliWayTokens = blackListPairString.Split(new string[] { "<->" }, StringSplitOptions.RemoveEmptyEntries);
                if (mutliWayTokens.Length == 2)
                {
                    yield return new UserNamePair()
                    {
                        SecretSantaUserName = (mutliWayTokens[0].Trim()),
                        ReceiverUserName = (mutliWayTokens[1].Trim())
                    };
                    yield return new UserNamePair()
                    {
                        SecretSantaUserName = (mutliWayTokens[1].Trim()),
                        ReceiverUserName = (mutliWayTokens[0].Trim())
                    };
                }
                else
                {
                       var tokens = blackListPairString.Split(new string[] { "->" }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length != 2)
                    continue;
                yield return new UserNamePair()
                {
                    SecretSantaUserName = (tokens[0].Trim()),
                    ReceiverUserName =(tokens[1].Trim())
                };
                }
            }
              
                
             
        } 

    }
}
