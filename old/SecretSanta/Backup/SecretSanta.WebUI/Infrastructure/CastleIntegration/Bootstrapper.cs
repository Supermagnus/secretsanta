using System.Web.Mvc;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using Castle.Windsor.Installer;
using SecretSanta.Core.Infrastructure;
using SecretSanta.Data.FileSystem.Infrastrucutre;
using SecretSanta.Data.Sql.Infrastructure;

namespace SecretSanta.WebUI.Infrastructure.CastleIntegration
{
    public static class Bootstrapper
    {

        public static IWindsorContainer CreateAndConfigureContainer()
        {
            var container = CreateAndConfigureBase();
            container.Install(
                FromAssembly.This(),
                new SqlInstaller(),
                new CoreInstaller()
                );

            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
          
            return container;
        }
        private static IWindsorContainer CreateAndConfigureBase()
        {
            var container = new WindsorContainer();
            container.Kernel.Resolver.AddSubResolver(new ArrayResolver(container.Kernel, true));
            return container;
        }
    }   
}