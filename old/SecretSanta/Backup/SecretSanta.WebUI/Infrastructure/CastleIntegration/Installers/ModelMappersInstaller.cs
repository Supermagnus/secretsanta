using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace SecretSanta.WebUI.Infrastructure.CastleIntegration
{
    public class ModelMappersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                AllTypes.FromAssemblyContaining<IModelMapper>()
                    .BasedOn<IModelMapper>()
                    .WithServiceFirstInterface()
                    .LifestyleSingleton()
                );



        }


    }
}