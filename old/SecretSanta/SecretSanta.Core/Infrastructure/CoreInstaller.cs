﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using SecretSanta.Core.ApplicationServices.Implementations;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Settings;

namespace SecretSanta.Core.Infrastructure
{
    public class CoreInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {


            container.Register(
                Component.For<EmailSettings>()
                    .ImplementedBy<EmailSettings>()
                );

            container.Register(
                Component.For<IEmailService>()
                .ImplementedBy<SmtpEmailService>()

                );


        }
    }
}
