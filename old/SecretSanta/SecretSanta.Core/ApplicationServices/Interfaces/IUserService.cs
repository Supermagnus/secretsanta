﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using SecretSanta.Core.Domain;

namespace SecretSanta.Core.ApplicationServices.Interfaces
{
    public interface IUserService
    {
        int SaveUser(User user);
        User GetUser(string userName);
        User GetUser(int userId);
        IList<User> GetAllUsers();
        void DeleteUser(string userName);
        void DeleteUser(int userId);

        void SaveUserMessage(UserMessage message, int fromUserId, int toUserId);
        /// <summary>
        /// Get all messages written to the given user Id. If you only want to see messages 
        /// from a conversation between two users, specify a fromUserId
        /// </summary>
        /// <returns></returns>
        IList<UserMessage> GetUserMessages(params MessageFilter[] filters);

        int SaveWish(Wish wish, int userId);
        void DeleteWish(int wishId, int userId);
        IList<Wish> GetWishes(int userId);
    }
}
