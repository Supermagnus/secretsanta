﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;

namespace SecretSanta.Core.ApplicationServices.Interfaces
{
    public interface IGroupService
    {
        /// <summary>
        /// Returns the Group id
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        int SaveGroup(Group group);

        Group GetGroup(string groupName);
        Group GetGroup(int groupId);

        IList<Group> GetAllGroups();
        
        IList<Group> GetGroupsForUser(string userName);
        IList<Group> GetGroupsForUser(int userId);
        void DeleteGroup(string groupName);
        void DeleteGroup(int groupId);

        /// <summary>
        /// Fetches the groups where the given userId is an administrator of
        /// </summary>
        /// <param name="usedId"></param>
        /// <returns></returns>
        IList<Group> GetAdministratedGroups(int usedId);

        void SaveGroupMessage(UserMessage message, int fromUserId, int toGroupId);
        IList<UserMessage> GetGroupMessages(int groupId, int maxMessages);
       
        IList<UserPair> GenerateUserPairs(Group group);
    }
}