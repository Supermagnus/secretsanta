﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.Core.Settings;

namespace SecretSanta.Core.ApplicationServices.Implementations
{
    public class SmtpEmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;

        public SmtpEmailService(EmailSettings emailSettings)
        {
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;

            _emailSettings = emailSettings;
        }

        public void SendLoginEmails(Group group, IList<User> users, string gameUrl)
        {
            var client = new SmtpClient(_emailSettings.SmtpServer, _emailSettings.SmtpServerPort)
            {
                Credentials = new NetworkCredential(_emailSettings.SendingEmail, _emailSettings.EmailPassword),
                EnableSsl = true
            };

            
            foreach (var user in users)
            {
                var header = "Välkommen till Hemliga tomten!";
                var body = "Du, just Du, har blivit utvald att få vara med i " + group.Name + " Hemliga Tomten.\r\n" +
                           "För att börja så öppnar du upp en webbläsare och går till " + gameUrl + ".\r\n" +
                           "\r\n" +
                           "Användarnamn: " + user.UserName + "\r\n" +
                           "Lösenord: " + user.Password + "\r\n" +
                           "\r\n" +
                           "Leken går ut på att du ska köpa en present till en slumpvis utvald person. Personen vet inte vem det är som är dess 'Hemliga tomte', bara du vet det!\r\n" +
                           "\r\n" +
                           "Har du några frågor kan du alltid kontaka mig. Jag finns här för Dig!" +
                           "\r\n" +
                           "\r\n" +
                           "Julvänlig hälsning,\r\n" +
                           "Din Magnus";

                var santa = " . . . . . _\r\n"+
" . . . . .{_}\r\n"+
" . . . . ./ \\\r\n"+
" . . . . / . \\\r\n"+
" . . . ./_____\\\r\n"+
" . . .{`_______`}\r\n"+
" . . . // . . \\\\\r\n"+
" . . .(/(__7__)\\)\r\n"+
" . . .|'-' = `-'|\r\n"+
" . . .| . . . . |\r\n"+
" . . ./\\ . . . /\\\r\n"+
" . . / .'. . .' .\\\r\n"+
" . ./_/ . `\"` . \\\\_\\\\\r\n"+
" . {__}###[_]###{__}\r\n"+
" . (_/\\_________/\\_)\r\n"+
" . . . |___|___|\r\n"+
" . . . .|--|--| \r\n"+
" . . . (__)`(__)";
                body += "\r\n\r\n" + santa;
                client.Send(_emailSettings.SendingEmail, user.Email, header, body);
                
            }
            
        }
    }
}
