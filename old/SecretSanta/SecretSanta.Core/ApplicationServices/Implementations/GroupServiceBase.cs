﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SecretSanta.Core.Domain;

namespace SecretSanta.Core.ApplicationServices.Implementations
{
    public abstract class GroupServiceBase
    {
        public IList<UserPair> GenerateUserPairs(Group group)
        {
            if (!group.UserIds.Any())
                return new List<UserPair>();
            int failedGenerations = 0;
            var rnd = new Random();
            var secretSantas = new List<int>(group.UserIds).OrderBy(x => rnd.Next()).ToList();
            var receivers = new List<int>(group.UserIds).OrderBy(x => rnd.Next()).ToList();
            var pairs = new List<UserPair>();
            while (secretSantas.Count + receivers.Count > 1)
            {
                if (failedGenerations > 100)
                    return GenerateUserPairs(group);

                secretSantas = secretSantas.OrderBy(x => new Random().Next()).ToList();
                receivers = receivers.OrderBy(x => new Random().Next()).ToList();

                var ss = secretSantas.FirstOrDefault();
                var receiver = receivers.FirstOrDefault();
                if (ss == receiver || group.BlacklistedPairs.Any(x => x.ReceiverId == receiver && x.SecretSantaId == ss))
                {
                    failedGenerations++;
                    continue;
                }
            
            pairs.Add(new UserPair()
                {
                    SecretSantaId = ss,
                    ReceiverId = receiver
                });
                secretSantas.Remove(ss);
                receivers.Remove(receiver);

                if (secretSantas.Count == 1 && receivers.Count == 1
                    && secretSantas[0] == receivers[0])
                    return GenerateUserPairs(group);
            }
            return pairs;
        }
    }
}
