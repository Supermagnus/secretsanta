﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace SecretSanta.Core.Domain
{
    public class User
    {
        public User()
        {
            UserName = "";
            Email = "";
            Password = "";
        }
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? LastLoginDate { get; set; }

        public string Password { get; set; }
    }
}
