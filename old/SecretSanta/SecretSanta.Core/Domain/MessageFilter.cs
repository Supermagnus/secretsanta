﻿namespace SecretSanta.Core.Domain
{
    public class MessageFilter
    {
        /// <summary>
        /// Get messages where this user is the receiver
        /// </summary>
        public int? ToUserId { get; set; }

        /// <summary>
        /// Get messages where this user is the send
        /// </summary>
        public int? FromUserId { get; set; }

        /// <summary>
        /// Limit the amount of messages to retrieve
        /// </summary>
        public int MaxMessages { get; set; }

        public static MessageFilter[] CreateUserConversationFilter(int user1Id, int user2Id)
        {
            var filters = new MessageFilter[2]
            {
                new MessageFilter()
                {
                    FromUserId = user1Id,
                    ToUserId = user2Id,
                    MaxMessages = 100
                },
                new MessageFilter()
                {
                    FromUserId = user2Id,
                    ToUserId = user1Id,
                    MaxMessages = 100
                }
            };
            return filters;
        }
}
}