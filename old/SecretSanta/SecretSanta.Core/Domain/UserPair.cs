﻿    namespace SecretSanta.Core.Domain
{
    public class UserPair
    {
        public int SecretSantaId { get; set; }
        public int ReceiverId { get; set; }
    }
}