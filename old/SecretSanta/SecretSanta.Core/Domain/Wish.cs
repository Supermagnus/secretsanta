﻿namespace SecretSanta.Core.Domain
{
    public class Wish
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}