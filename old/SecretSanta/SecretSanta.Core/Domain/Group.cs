﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SecretSanta.Core.Domain
{
    public class Group
    {
        public Group()
        {
            BlacklistedPairs = new List<UserPair>();
            GeneratedPairs = new List<UserPair>();
            UserIds = new List<int>();
            AdminUserIds= new List<int>();
            Name = "";
        }

        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Which users that have administrative right for this group
        /// </summary>
        public IList<int> AdminUserIds { get; set; }
        public IList<int> UserIds { get; set; } 

        public DateTime? Started { get; set; }
        public IList<UserPair> BlacklistedPairs { get; set; }
        public IList<UserPair> GeneratedPairs { get; set; }

      

        public UserPair GetPairWhereUserIsSecretSanta(int userId)
        {
            return GeneratedPairs.FirstOrDefault(x => x.SecretSantaId == userId);
        }

        public UserPair GetPairWhereUserIsReceiver(int userId)
        {
            return GeneratedPairs.FirstOrDefault(x => x.ReceiverId == userId);
        }

        public bool IsStarted()
        {
            return Started != null;
        }
    }
}