﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace SecretSanta.Core.Settings
{
    public class EmailSettings
    {
        public EmailSettings()
        {
            SmtpServer = ConfigurationManager.AppSettings["SmtpServer"];
            SmtpServerPort = int.Parse(ConfigurationManager.AppSettings["SmtpServerPort"]);
            SendingEmail = ConfigurationManager.AppSettings["SendingEmail"];
            EmailPassword = ConfigurationManager.AppSettings["EmailPassword"];
        }
        public string SmtpServer { get; set; }
        public int SmtpServerPort { get; set; }
        public string SendingEmail { get; set;   }
        public string EmailPassword { get; set; }
    }
}
